
public class Samochod {

	private Marka marka;
	private Cena cena;
	private Przebieg przebieg;
	private Rocznik rocznik;
	private Region region;
	private Miasto miasto;
	private CzyUszkodzony czyUszkodzony;
	private CzyNowy czyNowy;
	private DataDodaniaOgloszenia dataDodaniaOgloszenia;
	public Marka getMarka() {
		return marka;
	}
	public void setMarka(Marka marka) {
		this.marka = marka;
	}
	public Cena getCena() {
		return cena;
	}
	public void setCena(Cena cena) {
		this.cena = cena;
	}
	public Przebieg getPrzebieg() {
		return przebieg;
	}
	public void setPrzebieg(Przebieg przebieg) {
		this.przebieg = przebieg;
	}
	public Rocznik getRocznik() {
		return rocznik;
	}
	public void setRocznik(Rocznik rocznik) {
		this.rocznik = rocznik;
	}
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public Miasto getMiasto() {
		return miasto;
	}
	public void setMiasto(Miasto miasto) {
		this.miasto = miasto;
	}
	public CzyUszkodzony getCzyUszkodzony() {
		return czyUszkodzony;
	}
	public void setCzyUszkodzony(CzyUszkodzony czyUszkodzony) {
		this.czyUszkodzony = czyUszkodzony;
	}
	public CzyNowy getCzyNowy() {
		return czyNowy;
	}
	public void setCzyNowy(CzyNowy czyNowy) {
		this.czyNowy = czyNowy;
	}
	public DataDodaniaOgloszenia getDataDodaniaOgloszenia() {
		return dataDodaniaOgloszenia;
	}
	public void setDataDodaniaOgloszenia(DataDodaniaOgloszenia dataDodaniaOgloszenia) {
		this.dataDodaniaOgloszenia = dataDodaniaOgloszenia;
	}
}
