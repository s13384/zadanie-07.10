
public class DataDodaniaOgloszenia {

	private int dzien;
	private int miesiac;
	private int rok;
	
	public int getMiesiac() {
		return miesiac;
	}
	public void setMiesiac(int miesiac) {
		this.miesiac = miesiac;
	}
	public int getDzien() {
		return dzien;
	}
	public void setDzien(int dzien) {
		this.dzien = dzien;
	}
	public int getRok() {
		return rok;
	}
	public void setRok(int rok) {
		this.rok = rok;
	}
}
