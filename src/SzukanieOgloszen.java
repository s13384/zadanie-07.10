import java.util.ArrayList;


public class SzukanieOgloszen implements ISzukanieOgloszen {

	@Override
	public ArrayList<Samochod> szukanieOgloszen(ArrayList<Samochod> samochody,
			Samochod kryteria) {
		
		// do listy wynikow dodajemy wszyskie samochody ktore potem zaleznie od kryteriow beda usuwane
		ArrayList<Samochod> wynik=samochody;
		int i,j;
		
		
		if (kryteria.getMarka().getMarka() != null)
		{
			for (i=0;i<wynik.size();i++)
			{
				if (wynik.get(i).getMarka().getMarka() != kryteria.getMarka().getMarka())
				{
					wynik.remove(i);
				}
			}
		}
		
		// widelki cenowe od 80% ceny wpisanej do 120%
		for (i=0;i<wynik.size();i++)
		{
			if (wynik.get(i).getCena().getCena() > kryteria.getCena().getCena()*12/10 || wynik.get(i).getCena().getCena() < kryteria.getCena().getCena()*8/10 )
			{
				wynik.remove(i);
			}
		}
		// widelki przebiegu od 80% do 120%
		for (i=0;i<wynik.size();i++)
		{
			if (wynik.get(i).getPrzebieg().getPrzebieg() > kryteria.getPrzebieg().getPrzebieg()*12/10 || wynik.get(i).getPrzebieg().getPrzebieg() < kryteria.getPrzebieg().getPrzebieg()*8/10 )
			{
				wynik.remove(i);
			}
		}
		// uswanie innych rocznikow
		for (i=0;i<wynik.size();i++)
		{
			if (wynik.get(i).getRocznik().getRocznik() != kryteria.getRocznik().getRocznik())
			{
				wynik.remove(i);
			}
		}
		
		if (kryteria.getRegion().getRegion() != null)
		{
			for (i=0;i<wynik.size();i++)
			{
				if (wynik.get(i).getRegion().getRegion() != kryteria.getRegion().getRegion())
				{
					wynik.remove(i);
				}
			}
		}
		
		if (kryteria.getMiasto().getMaisto() != null)
		{
			for (i=0;i<wynik.size();i++)
			{
				if (wynik.get(i).getMiasto().getMaisto() != kryteria.getMiasto().getMaisto())
				{
					wynik.remove(i);
				}
			}
		}
		
		// jezeli samochod moze byc uszkodzony to nic nie robimy , jak nie moze to usuwamy uszkodzone
		if (! kryteria.getCzyUszkodzony().isCzyUszkodzony())
		{
			for (i=0;i<wynik.size();i++)
			{
				if (wynik.get(i).getCzyUszkodzony().isCzyUszkodzony())
				{
					wynik.remove(i);
				}
			}
		}
		
		// jezeli me byc nowy to usuwamy wszystkie stare
		if (kryteria.getCzyNowy().isCzyNowy())
		{
			for (i=0;i<wynik.size();i++)
			{
				if (!wynik.get(i).getCzyNowy().isCzyNowy())
				{
					wynik.remove(i);
				}
			}
		}
				
		return wynik;
	}

}
