import java.util.ArrayList;

// nieuzywane
public interface IPrzeszukiwanie {

	public void  szukajOgloszen(ArrayList<Samochod> samochody, Marka marka);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, Cena cena);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, Przebieg przebieg);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, Rocznik rocznik);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, Region region);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, Miasto miasto);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, CzyUszkodzony czyUszkodzony);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, CzyNowy czyNowy);
	
	public void szukajOgloszen(ArrayList<Samochod> samochody, DataDodaniaOgloszenia dataDodaniaOgloszenia);
}
