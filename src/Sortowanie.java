import java.util.ArrayList;


public class Sortowanie implements ISortowanie {

	@Override
	public void sortowanie(ArrayList<Samochod> wyniki , String metodaSortowania) {
		
		int i,j;
		
		for (i=0;i<wyniki.size();i++)
		{
			for (j=0;j<wyniki.size();j++)
			{
				if (metodaSortowania == "po cenie")
				{
					if (wyniki.get(i).getCena().getCena()>wyniki.get(j).getCena().getCena())
					{
						swap(wyniki,i,j);
					}
				}
				else if (metodaSortowania == "po roczniku")
				{
					if (wyniki.get(i).getRocznik().getRocznik()>wyniki.get(j).getRocznik().getRocznik())
					{
						swap(wyniki,i,j);
					}
				}
				else if (metodaSortowania == "po dacie dodania")
				{
					if (wyniki.get(i).getDataDodaniaOgloszenia().getRok()>wyniki.get(j).getDataDodaniaOgloszenia().getRok())
					{
						swap(wyniki,i,j);
					}
					else if (wyniki.get(i).getDataDodaniaOgloszenia().getRok() == wyniki.get(j).getDataDodaniaOgloszenia().getRok())
					{
						if (wyniki.get(i).getDataDodaniaOgloszenia().getMiesiac()>wyniki.get(j).getDataDodaniaOgloszenia().getMiesiac())
						{
							swap(wyniki,i,j);
						}
						else if (wyniki.get(i).getDataDodaniaOgloszenia().getDzien()>wyniki.get(j).getDataDodaniaOgloszenia().getDzien())
						{
							swap(wyniki,i,j);
						}
					}
				}
				else if (metodaSortowania == "po przebiegu")
				{
					if (wyniki.get(i).getPrzebieg().getPrzebieg()>wyniki.get(j).getPrzebieg().getPrzebieg())
					{
						swap(wyniki,i,j);
					}
				}
			}
		}

	}

	private void swap(ArrayList<Samochod> wyniki, int index1, int index2) {
		
		Samochod pomocniczy;
		pomocniczy=wyniki.get(index1);
		wyniki.set(index1, wyniki.get(index2));
		wyniki.set(index2, pomocniczy);
		
	}

}
